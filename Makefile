# Makefile for switch-config

CC		=	$(CROSS_COMPILE)gcc
KBUILD_OUTPUT	?=	/lib/modules/$(shell uname -r)/build
CFLAGS		=	-I$(KBUILD_OUTPUT)/usr/include

GIT_VERSION := $(shell git describe --dirty --always)
RM = rm -f
CFLAGS+= -Wall -DVERSION=\"$(GIT_VERSION)\"
LDFLAGS+=

PROG = switch-config
SRCS = switch-config.c

OBJS = $(SRCS:.c=.o)

HDRS =

.c.o:
	$(CC) -c $(CFLAGS) -o $@ $<

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

$(OBJS): $(HDRS)

clean:
	$(RM) $(PROG) $(OBJS)

help:
	@echo "make CROSS_COMPILE=arm-linux-gnueabihf- KBUILD_OUTPUT=<Kernel Directory>"
